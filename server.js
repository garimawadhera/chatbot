'use strict'

const express = require('express');
const bodyParser = require('body-parser');
var mongoose = require('mongoose');
const app = express();

var facebook_parser = require('./utils/bot_utils.js');
var fs = require('fs');
var path = require('path');
var uuid = require('uuid');
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.set('port', (process.env.PORT || 3000));
const MONGO_HOST = (process.env.MONGO_HOST || 'localhost');
app.set('mongo_url', (process.env.MONGODB_URL || 'mongodb://'+MONGO_HOST+'/local'));

/* Redirect http to https */
app.get('*', function (req,res,next) {
    if (req.headers['x-forwarded-proto'] != 'https' && process.env.NODE_ENV === 'production')
        res.redirect('https://' + req.hostname + req.url);
    else
        next(); /* Continue to other routes if we're not redirecting */
});

// serve static files from 'css' (stylesheets), 'scripts' (client-side JS) and 'assets' (images, etc.) directories
app.use(express.static('scripts'));
app.use(express.static('css'));

mongoose.connect(app.get('mongo_url'),function(err){
	if (err) {
		console.error(err);
		process.exit(1);
	}
	console.log("connected to " + app.get('mongo_url'));
});

// Parse application/json
app.use(bodyParser.json());

// Parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// Index route
app.get('/', function (req, res) {
    return res.sendFile(path.join(__dirname+'/www/index.html'));
});

// router to send API keys to client side
app.get('/config', function (req, res) {
    res.json({
        "pizzabot": process.env.PIZZABOT_API_KEY,
        "facebook": {
            "appID": process.env.FACEBOOK_APP_ID,
            "pageID": process.env.FACEBOOK_PAGE_ID
        },
        "google": {
            "mapsAPI": process.env.GOOGLE_MAPS_API_KEY
        }
    })
})

// socket.io listener for connections
io.on('connection', function (socket) {
    // generate uuid for session
    var session_uuid = uuid.v4();
    // send connected socket to private chat room so that we can safely pass data from auth event
    socket.join(session_uuid);
    // send UUID to connected socket
    socket.emit('response', { 'uuid' : session_uuid } );
});

// for Facebook verification
app.get('/webhook/', function (req, res) {
    if (req.query['hub.verify_token'] === process.env.FACEBOOK_VERIFY_TOKEN) {
        res.send(req.query['hub.challenge']);
    }
    res.send('Error, wrong token');
});

var products = require('./menu.json'); // product data stored on the server


// handle a URI encoded JSON request as a GET
app.get('/api/products', function (req, res) {
    var reqJSON = JSON.parse(decodeURIComponent(req.query.orderData));
    var orderDetails = reqJSON.order;
    var response = {"item_data" : []};
    var subtotal = 0;
    for (var j = 0; j < orderDetails.length; j++) {
        var item = orderDetails[j];
        var product = products.products[item.productID];
        var title = product.title;
        var subtitle = "";
        var product_image = product.image_url;
        var price = product.price;
        var extras = item.extras;
        for (var i = 0; i < extras.length; i++) {
            var extra = products.extras[extras[i]];
            price += extra.price;
            if (extras.length === 1 || i == extras.length - 2) {
                subtitle += extra.title;
            } else if (i === extras.length - 1) {
                subtitle += ' and ' + extra.title;
            } else {
                subtitle += extra.title + ", ";
            }
        }
        subtotal += item.quantity * price;
        response.item_data.push({
            "title" : title,
            "subtitle" : subtitle,
            "price" : price,
            "quantity" : item.quantity,
            "image_url" : product_image
        });
    }
    var total_tax = 0.0825 * subtotal;
    var total_cost = subtotal + total_tax;
    response.summary = {
        "subtotal" : subtotal.toFixed(2),
        "total_tax" : total_tax.toFixed(2),
        "total_cost" : total_cost.toFixed(2)
    };
    res.json(response);
});

// API endpoint to process messages
var myURL;
app.post('/webhook/', function (req, res) {
    myURL = req.protocol + '://' + req.get('host');
    messaging_events = req.body.entry[0].messaging;
    for (i = 0; i < messaging_events.length; i++) {
        event = req.body.entry[0].messaging[i];
        sender = event.sender.id;
        if (event.message && event.message.text) {
            text = event.message.text;
            var upperCasedText = text.toUpperCase();
            if (upperCasedText.includes('ORDER PIZZA')) {
                sendPizzaCTA(sender);
                continue;
            } else if (upperCasedText.includes('AWESOME')) {
                sendTextMessage(sender, "Glad you liked it! \ud83d\ude0a");
                continue
            } else if (upperCasedText.includes('LOVE IT')) {
                sendTextMessage(sender, 'We love it too! \ud83d\ude0d');
                continue
            } else if (upperCasedText.includes('BLAH')) {
                sendTextMessage(sender, 'Aw, sorry you didn\'t like it! \ud83d\ude1f');
                continue
            } else if (upperCasedText.includes('FLIGHT')) {
                sendAirlineTemplate(sender);
                continue
            }
            sendTextMessage(sender, "Unfortunately I couldn't quite figure out what you were saying, but here's a part of it: " + text.substring(0, 200))
            if (event.postback) {
                text = JSON.stringify(event.postback)
                sendTextMessage(sender, "Postback received:" + text.substring(0, 200))
                continue
            }
        }
    }
    res.sendStatus(200);
});




// Server listener
app.listen(app.get('port'), function() {
	console.log('Facebook Messenger server started on port', app.get('port'));
});
